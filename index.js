
$( document ).ready(function() {
    var arr = [];
    var i = 0;
    var j = 0;
    var totalltv = 0;
    var totaltran = 0;
    var totalpoint = 0;
    var remainpoint = 0;
    fetch('https://wegivmerchantapp.firebaseapp.com/exam/bi-member-day-2020-04-01.json')
    .then(res => res.json())
    .then( function(data) {

        data.data.list.forEach(function(listData) {
            
            if(i==0){
                arr.push(listData.customername);
                
                $('#datatable').find('tbody').append("<tr>"+
                "<td style='text-align: left;'>"+listData.customername+"</td>"+
                "<td style='text-align: center;'>"+listData.customerphone+"</td>"+
                "<td style='text-align: center;'>"+listData.customertier+"</td>"+
                "<td style='text-align: right;'>"+listData.totalamount+"</td>"+
                "<td style='text-align: right;'>"+listData.totaltransaction+"</td>"+
                "<td style='text-align: right;'>"+listData.totalreward+"</td>"+
                "<td style='text-align: right;'>"+listData.remainingpoint+"</td>"+
                "</tr>");
                j++;

                totalltv=listData.totalamount;
                totaltran=listData.totaltransaction;
                totalpoint=listData.totalreward;
                remainpoint=listData.remainingpoint;

            } else {
                var ch = true;
                for(var index = 0; index < arr.length ; index++){
                    if(listData.customername == arr[index]){
                        ch = false;
                    }
                }
                
                if(ch) {
                    arr.push(listData.customername);

                    $('#datatable').find('tbody').append("<tr>"+
                    "<td style='text-align: left;'>"+listData.customername+"</td>"+
                    "<td style='text-align: center;'>"+listData.customerphone+"</td>"+
                    "<td style='text-align: center;'>"+listData.customertier+"</td>"+
                    "<td style='text-align: right;'>"+listData.totalamount+"</td>"+
                    "<td style='text-align: right;'>"+listData.totaltransaction+"</td>"+
                    "<td style='text-align: right;'>"+listData.totalreward+"</td>"+
                    "<td style='text-align: right;'>"+listData.remainingpoint+"</td>"+
                    "</tr>");
                    j++;
                  
                    totalltv+=listData.totalamount;
                    totaltran+=listData.totaltransaction;
                    totalpoint+=listData.totalreward;
                    remainpoint+=listData.remainingpoint;
                    
                }
            }
            $('#totalltv').text(totalltv);
            $('#totaltran').text(totaltran);
            $('#totalpoint').text(totalpoint);
            $('#remainpoint').text(remainpoint);
            i++;
        });
        $('#fname').width($('#tname').width());
        $('#fid').width($('#tid').width());
        $('#ftier').width($('#ttier').width());
        $('#fltv').width($('#tltv').width());
        $('#ftrans').width($('#ttrans').width());
        $('#fpoint').width($('#tpoint').width());
        $('#frepoint').width($('#trepoint').width());
        $('#totalmember').text(j);
        $('#totalmember2').text(j);
        if(totalltv=>1000){
            var sum = totalltv/1000
            $('#totalrev').text(parseInt(sum)+"K");
            $('#totalrev2').text(parseInt(sum)+"K");
        }
        
    })
    .catch(err => { throw err });
});

